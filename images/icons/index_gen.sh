#!/bin/bash

icon=(*.png)

cat head.html > index.html

echo -n "<section class=\"deck\">" >> index.html
echo -n "<p>${#icon[@]}</p>" >> index.html
for ((d=0; d<${#icon[@]}; d++)); do
    echo -n "<article class=\"small inline-block\"><img src=\"${icon[$d]}\"/></article>" >> index.html
done
echo -n "</section>" >> index.html
echo -n "<section>" >> index.html
for ((d=0; d<${#icon[@]}; d++)); do
    echo -n "<article class=\"big inline-block\"><img src=\"${icon[$d]}\"/><legend>${icon[$d]::-4}</legend></article>" >> index.html
done
echo -n "</section>" >> index.html
