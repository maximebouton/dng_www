#!/bin/bash

dir=(*/)

cat head.html > index.html

for ((d=0; d<${#dir[@]}; d++)); do
    if [ -e ${dir[$d]}/vignette.txt ]; then
        if [ -e ${dir[$d]}/index.html ]; then
            echo -n "<pre><a href=\"${dir[$d]}/index.html\">$(cat ${dir[$d]}/vignette.txt)</a></pre>" >> index.html
        else
            echo -n "<pre><a href=\"under_construction.html\">$(cat ${dir[$d]}/vignette.txt)</a></pre>" >> index.html
        fi
    else 
        echo no vignette
        echo -n "<pre><a href=\"under_construction.html\">$(cat blank.txt)</a></pre>" >> index.html
    fi
done
